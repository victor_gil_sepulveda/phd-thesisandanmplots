'''
Created on Oct 1, 2015

@author: victor
'''

import plots.anm_paper_plots.data as data_module
from plots.anm_paper_plots.data import data
import matplotlib.pyplot as plt
import numpy
import os
import random
from plots.tools import define_tableau20_cm
import plots.tools as tools
#import ccvsic.calculate_ccvsic_data as ccvsic_tools
import seaborn as sns
import matplotlib
import pandas as pd
from _collections import defaultdict

data_path = data_module.__path__[0]

def plot_ncmc_md_energies(the_file):
    print the_file
    energy_data = numpy.loadtxt(os.path.join(data_path, "source", the_file))
    file_info = data.md_energies_NCMC[the_file]
    def get_data_ranges(info, e_data):
        steps_md = info["md_steps"]
        steps_rx = info["relax_steps"]*info["relax_iterations"]
        steps_cycle = steps_md + steps_rx
        cycle_ranges = []
        for i in range(0, len(e_data)/steps_cycle):
            cycle_ranges.append(((i*steps_cycle, (i+1)*steps_cycle), i))
        return cycle_ranges
    
    cycle_ranges = get_data_ranges(file_info, energy_data)
    ranges_to_draw = [0,436]#,1]
#     for _ in range(8):
#         ranges_to_draw.append(random.randint(2,len(cycle_ranges)-1))
    
    colors = define_tableau20_cm()
    
    for selected_range in ranges_to_draw:
        fig = plt.figure(figsize=(8, 6)) 
        
        gs = plt.GridSpec(5,1) 
        ax1 = plt.subplot(gs[0:4,:])
        ax2 = ax1.twinx()
        
        data_range = range(*cycle_ranges[selected_range][0])
        plt.title("MD energy %d"%selected_range)
        ax1.set_ylabel("U")
        ax1.plot(energy_data.T[1][data_range],color=colors[0], label='U')
        ax1.plot(energy_data.T[2][data_range]+min(energy_data.T[1][data_range]), color=colors[1], label='U_c')
        ax1.legend()
        ax2.set_ylabel("K")
        ax2.plot(energy_data.T[0][data_range],color=colors[2], label='K')
        ax2.legend(loc=2)

        
        ax3 = plt.subplot(gs[4,:])
        ax3.plot(energy_data.T[0][data_range]+energy_data.T[1][data_range],color=colors[3])
        ax3.plot(energy_data.T[0][data_range]+energy_data.T[1][data_range]-energy_data.T[2][data_range],color=colors[4])
        
        
        plt.show()
        #plt.savefig("%s_%s.svg"%(os.path.basename(the_file),selected_range))
    

def plot_anm_energy_increment_vs_rmsd(data_folders):
    data = {}
    colors = sns.color_palette("hls", 8)
    #colors = define_tableau20_cm()
    inc_U = {}
    rmsd = {}
    for i,folder in enumerate(data_folders):
        data = ccvsic_tools.load_data(folder)
        inc_U[folder] = ccvsic_tools.process_energy_differences(data)[1:]
        rmsd[folder] = ccvsic_tools.process_after_perturb_rmsd(data)[1:]
        print "SIZES U: ",len(inc_U[folder]), "rmsd",len(rmsd[folder])

#     pd_data_dic = defaultdict(list)
#     for folder in data_folders:
#         data = ccvsic_tools.load_data(folder)
#         u_data = ccvsic_tools.process_energy_differences(data)[1:]
#         pd_data_dic["inc_U"].extend(u_data)
#         rmsd_data = ccvsic_tools.process_after_perturb_rmsd(data)[1:len(u_data)+1]
#         pd_data_dic["rmsd"].extend(rmsd_data)
#         pd_data_dic["folder"].extend([os.path.basename(folder)]*len(rmsd_data))
#      
#     pd_data = pd.DataFrame.from_dict(pd_data_dic)
#     g = sns.FacetGrid(pd_data, col="folder")
#     g.map(plt.hexbin, "rmsd","inc_U")
# #     sns.jointplot(x="total_bill", y="tip", hue data=pd_data)
#     plt.legend()
#     plt.show()
    for i,folder in enumerate(data_folders):
        plt.scatter(rmsd[folder][:len(inc_U[folder])], inc_U[folder],  color=colors[i], label = os.path.basename(folder), alpha = 0.8)
    plt.legend()    
    plt.show()
     
    for i,folder in enumerate(data_folders):
        plt.hist(rmsd[folder][:len(inc_U[folder])], color=colors[i], label = os.path.basename(folder), alpha = 0.8, bins = 50, normed=True)
    plt.legend()    
    plt.show()
     
    for i,folder in enumerate(data_folders):
        plt.hist(inc_U[folder], color=colors[i], label = os.path.basename(folder), alpha = 0.8,bins = 50, normed=True, orientation="horizontal")
    plt.legend()    
    plt.show()

     
def plot_CC_vs_MD(csv_filename):
   
    data = tools.import_csv_data(csv_filename)
    all_by_T, T_keys, T_colors = tools.sort_data_by_key("T", data, len(data["rmsf"]))
   
    data_labels = {"acc":"Acceptance","rmsf":"rms(rmsf)","sasa":"JSD(SASA)","rgyr":"JSD(Radius of gyration)","Ca_disp":"$C_\\alpha$ disp"}
    def plt_in_subplot(data_by_hue, hue_key, hue_keys_vals, hue_colors,
                       x_key, y_key, axis_labels, grid_spec, position):
        ax = plt.subplot2grid(grid_spec,position)
        for hue_key_val in hue_keys_vals:
            plt.scatter(data_by_hue[hue_key_val][x_key],data_by_hue[hue_key_val][y_key], 
                        label = "%s = %s"%(hue_key, str(hue_key_val)),color=hue_colors[hue_key_val])
        ax.set_xticks(numpy.arange(0,1,0.1))
        plt.xlabel(axis_labels[x_key])
        plt.ylabel(axis_labels[y_key])
    
    
    plt_in_subplot(all_by_T, "T", T_keys, T_colors,
                   "acc", "Ca_disp", data_labels, (2,2), (0, 0))
    plt_in_subplot(all_by_T, "T", T_keys, T_colors,
                   "acc", "rmsf", data_labels, (2,2), (1, 0))
    plt_in_subplot(all_by_T, "T", T_keys, T_colors,
                   "acc", "sasa", data_labels, (2,2), (0, 1))
    plt_in_subplot(all_by_T, "T", T_keys, T_colors,
                   "acc", "rgyr", data_labels, (2,2), (1, 1))
    plt.tight_layout()

    legend_patches = [matplotlib.lines.Line2D(range(1), range(1), color="white", marker='o', markerfacecolor=T_colors[t]) for t in T_keys]
    plt.figlegend( legend_patches,
                  ["%s"%(str(t)) for t in T_keys], 
                   loc = 9,ncol=len(T_keys))
    sns.plt.show()
    
    
    pd_data = pd.read_csv(csv_filename)
#     pd_data = pd_data[pd_data["T"]!=2568] # remove incomplete data
     
    # acceptance by Ca dist and T
    pd_data = pd.read_csv(csv_filename)
    ax = sns.barplot(x="T", y="acc", hue="Ca_disp", data=pd_data)
    margin = 0.5
    ax.add_patch(
        matplotlib.patches.Rectangle(
            (-margin*2, 0.25),
            5+(margin*2),0.1,
            fill=False,      # remove background
            lw=2,
            ls = "dotted",
            alpha = 0.5
            )
        )                
    plt.show()
     
    # rmsf vs  T and colored by Ca_dist
    ax = sns.barplot(x="T", y="rmsf", hue="Ca_disp", data=pd_data, orient = "v")
    plt.ylim(ymin=0.6)
    plt.show()
 
    # sasa vs  T and colored by Ca_dist
    ax = sns.barplot(x="T", y="sasa", hue="Ca_disp", data=pd_data, orient = "v")
    plt.ylim(ymin=0.6)
    plt.show()
     
    # rgyr vs  T and colored by Ca_dist
    ax = sns.barplot(x="T", y="rgyr", hue="Ca_disp", data=pd_data, orient = "v")
    plt.ylim(ymin=0.6)
    plt.show()

def plot_mode_cum_overlap_and_rmsip(csv_cum_overlap, csv_rmsip):
    pd_data = pd.read_csv(csv_cum_overlap).transpose()
    pd_data.plot(kind='bar')
    pd_data_rmsip = pd.read_csv(csv_rmsip)
    y = []
    for prot in pd_data_rmsip.columns:
        y.append(pd_data_rmsip[prot])
    plt.plot(range(len(y)),y, marker = "o", lw =5, ms = 10, c = "black")
    plt.ylim((0.38,1))
    plt.show()
#     ax = sns.barplot(x="0", y="acc", hue="Ca_disp", data=pd_data)

def plot_collectivity(csv_collectivity):
    sns.set_style("whitegrid")
    pd_data = pd.read_csv(csv_collectivity).transpose()
    pd_data.plot(kind='bar')
    plt.show()
    
def plot_collectivity_averages(csv_collectivity_cc, csv_collectivity_ic):
    colors = sns.color_palette("hls", 8)
    ordered_columns = ["1ddt", "1ex6", "4ake", "1ggg", "src_kin", "src_kin_2", "1su4"]
    pd_data_cc = pd.read_csv(csv_collectivity_cc)
    pd_data_cc = pd_data_cc[ordered_columns]
    pd_data_ic = pd.read_csv(csv_collectivity_ic)
    pd_data_ic = pd_data_ic[ordered_columns]
    sns.set_style("whitegrid")
    fig, ax = plt.subplots()
#     plt.plot(range(len(pd_data_cc.columns)),pd_data_cc.mean(), label = "CC", marker = "o")
#     plt.plot(range(len(pd_data_cc.columns)),pd_data_ic.mean(), label = "IC", marker = "o")
    plt.errorbar(range(len(pd_data_cc.columns)), pd_data_cc.mean(), yerr= pd_data_cc.std(), 
                 label = "CC", alpha=0.7, mew=2, lw=3, ms=5, marker = "o", markeredgecolor = colors[0], color = colors[0])
    plt.errorbar(range(len(pd_data_ic.columns)), pd_data_ic.mean(), yerr= pd_data_ic.std(), 
                 label = "IC", alpha=0.7, mew=2, lw=3, ms=5, marker = "o", markeredgecolor = colors[1], color = colors[1])
    ax.set_xticklabels(pd_data_ic.columns)
    plt.legend()
    plt.show()

if __name__ == '__main__':
    print data_path
#     plot_md_energies("3s_md_energies.log")
#     plot_md_energies("verlet_md_energies.log")
#     plot_md_energies("only_closing_energies.log")
#     plot_md_energies("free_relax_md_energies.log")
#     plot_md_energies("frozen_relax_md_0.05_energies.log")
#     plot_md_energies("test_md.log")
#     plot_ncmc_md_energies("3s_md_energies.log")
#     plot_ncmc_md_energies("verlet_md_energies.log")
#     plot_anm_energy_increment_vs_rmsd([
# #                                        "/home/victor/Desktop/CCvsIC/CCvsIC_2568_1_5",
# #                                        "/home/victor/Desktop/CCvsIC/CCvsIC_2000_1_08",
# #                                        "/home/victor/Desktop/CCvsIC/ICvsCC_300_0_1_15",
#                                         "/home/victor/VBShared/CCvsIC/CCvsIC_2568_1_5/info",
#                                         "/home/victor/VBShared/CCvsIC/CCvsIC_2000_1_08/info",
#                                         "/home/victor/VBShared/CCvsIC/ICvsCC_300_0_1_15/info",
#                                        ])
#     
    plot_CC_vs_MD(os.path.join(data_path,"source","comparison","CCvsMD.csv"))

#     plot_mode_cum_overlap_and_rmsip(os.path.join(data_path,"source","mode_comparison","cumulative_overlap.csv"),
#                                     os.path.join(data_path,"source","mode_comparison","rmsip.csv"))
#     plot_collectivity(os.path.join(data_path,"source","mode_comparison","doc_cc.csv"))
#     plot_collectivity(os.path.join(data_path,"source","mode_comparison","doc_ic.csv"))
    plot_collectivity_averages(os.path.join(data_path,"source","mode_comparison","doc_cc.csv"),
                               os.path.join(data_path,"source","mode_comparison","doc_ic.csv"))