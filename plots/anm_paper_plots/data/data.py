'''
Created on Oct 21, 2015

@author: victor
'''
md_energies_NCMC = {
                    
                    "3s_md_energies.log": {"md_steps": 120, "relax_steps": 50, "relax_iterations":10+1},
                    "verlet_md_energies.log": {"md_steps": 360, "relax_steps": 50, "relax_iterations":10+1},
                    "only_closing_energies.log": {"md_steps": 360, "relax_steps": 50, "relax_iterations":10+1},
                    "free_relax_md_energies.log": {"md_steps": 200, "relax_steps": 50, "relax_iterations":10+1},
                    "frozen_relax_md_0.05_energies.log": {"md_steps": 200, "relax_steps": 50, "relax_iterations":10+1},
                    "frozen_relax_md_0.1_energies.log": {"md_steps": 200, "relax_steps": 50, "relax_iterations":10+1},
                    "test_md.log": {"md_steps": 10, "relax_steps": 10, "relax_iterations":1+1}
                    
                    }