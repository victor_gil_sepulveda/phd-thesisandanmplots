
NME_cost_per_year = {
                     "average": [(1970 ,0.199),
                                (1980,0.413),
                                (2003, 0.800),
                                (2004, 1.1),
                                (2006, 0.800),
                                (2009, 1.7),
                                (2010, 1.78),
                                (2011, 1.2),
                                (2012, 1.3),
                                (2013, 2.6),
                                (2014, 2.9)],

                     "extreme": [
                                (2012, 4),
                                (2012, 11),
                                (2013, 5)]
                     }

# Cost of 1 NME
# ---------------
# 1970 $199 million  (The R&D Cost of a New Medicine Mestre-Ferrandiz, J.)
# 1980 $413 million  (PhRMA 2015)
# 2003 $800 million  (DiMassi) 
# 2004 $1.1 million  (Tufts Center for the Study of Drug Development from Spotfire ref) 
# 2006 $800 million  (Gwynne) 
# 2009 $1.7 billion  (Tufts Center for the Study of Drug Development from Spotfire ref) 
# 2010 $1.78 billion (Steven M. Paul)
# 2011 $1.2 billion  (Phrma annual 2011) 
# 2012 $1.3 billion  (http://lillypad.lilly.com/entry.php?e=1424) 
# 2013 $2.6 billion  (DiMassi 2013, http://www.nejm.org/doi/full/10.1056/NEJMc1504317) , 
# 2014 $2.9 billion  (http://www.scientificamerican.com/article/cost-to-develop-new-pharmaceutical-drug-now-exceeds-2-5b/)
# 2012 $4-$11 billion (Dreyfus or  Herper (http://www.forbes.com/sites/matthewherper/2013/08/11/how-the-staggering-cost-of-inventing-new-drugs-is-shaping-the-future-of-medicine/)),  
# 2013 $5 billion     (companies with more drugs accepted, from Herper) 
# 
# Time for dev 1 NME
# -------------------
# 
# 1970 6 years (Dreifus)
# 2000 13.5 years (Dreifus)
# 2006 15 years (Gwynne) 
# 2010 15 years (Steven M. Paul)
NME_time = [(1970,6),(2000,13.5),(2006,15),(2010, 15)]

# Initial profiling
initial_profiling = {
                      
                     "Small": {
                             "OBC":{
                                    "Solvent":11.5,
                                    "NB":50.7
                                    },
                             "SGB":{
                                    "Solvent":58.94,
                                    "NB":25.53
                                    }
                             },
                     "Medium": {
                             "OBC":{
                                    "Solvent": 39.67,
                                    "NB": 20.21 
                                    },
                             "SGB":{
                                    "Solvent": 64.44,
                                    "NB": 11.28
                                    }
                             },
                     "Big": {
                             "OBC":{
                                    "Solvent": 11.5,
                                    "NB": 35.65+14.8+13.64
                                    },
                             "SGB":{
                                    "Solvent": 58.84,
                                    "NB": 17.33 + 8.2
                                    }
                             }
                     }
 
NB_kernel_speedup = {
                     "Small":{
                              "CUDA":{
                                     "energy": 15,
                                     "gradient": 8.7 
                                     },
                              "OpenCL":{
                                     "energy": 15,
                                     "gradient": 8.7
                                     }
                              },
                     "Medium":{
                              "CUDA":{
                                     "energy": 24.8,
                                     "gradient": 12.1
                                     },
                              "OpenCL":{
                                     "energy": 19.8,
                                     "gradient": 9.1 
                                     }
                              }
                     }

# NB_program_speedups = [
# # S    M    B
# #CUDA
# [1.19,1.37,1.32], #(N,M) (1,20)
# [1.16,1.34,1.27], #(65,50)
# 
# #OpenCL
# [1.21,1.39,1.33], #(1,20)
# [1.19,1.36,1.30], #(65,50)
# 
# #Theoretical max.
# [1.35,1.47,1.38]
# ]
# 
Pharmacelera_alpha_speedup = {
                              "Medium":[ #Method 1, 2 and 3 
                                        3.7,
                                        14.6,
                                        15.6
                                        ],
                               
                              "Big":[ #Method 1, 2 and 3
                                     4.1,
                                     13.6,
                                     14.5
                                     ]
                              }
