'''
Created on Oct 1, 2015

@author: victor
'''

import data as data_module
import data.data as _data
import matplotlib.pyplot as plt
import numpy
import os
import sklearn.datasets
import sklearn.cluster
from plots.tools import define_tableau20_cm
import pandas as pd
import seaborn as sns
sns.set_style("whitegrid")

data_path = data_module.__path__[0]

def plot_drug_cost_per_year():
    
   
    y_time = numpy.array([ d[0] for d in _data.NME_time])
    time = numpy.array([ d[1] for d in _data.NME_time])
    plt.bar(y_time, time, 1)
    plt.xlabel("Year")
    plt.ylabel("Time (years)")
    
    ax = plt.gca().twinx()
    years = numpy.array([ d[0] for d in _data.NME_cost_per_year["average"]])
    costs = numpy.array([ d[1] for d in _data.NME_cost_per_year["average"]])
    ax.plot(years, costs)
    ax.plot(years, costs, marker='o')#, edgecolor='black', linewidth='3', facecolor='green')
    ax.set_ylabel("Cost ($ billions)")
    plt.show()
    

def plot_3adk_closing_cumulative_overlap():
    data_source = os.path.join(data_path, "source", "closing_cumulative_overlap")
    distances = numpy.loadtxt(os.path.join(data_source,"distances.txt"))
    cum_overlaps_wr = numpy.loadtxt(os.path.join(data_source,"cum_overlap_with_first_ref.txt"))
    sing_max_overlaps_wr = numpy.loadtxt(os.path.join(data_source,"max_sing_ov_with_first_ref.txt"))
    cum_overlaps_wt = numpy.loadtxt(os.path.join(data_source,"cum_overlap_with_trans.txt"))
    sing_max_overlaps_wt = numpy.loadtxt(os.path.join(data_source,"max_sing_ov_with_trans.txt"))
    
    def plot_fig(cum_overlaps, max_overlaps, x_tick_labels):    
        fig = plt.figure()
        ax = fig.add_subplot(111)
        for i in range(1,len(cum_overlaps.T)):
            plt.plot(cum_overlaps.T[0], cum_overlaps.T[i]*100, label = "Mode %d"%i)
        plt.plot(cum_overlaps.T[0], max_overlaps.T[0]*100, label = "Max. overlap", lw = 4.0)
        for i, xy in enumerate(zip(cum_overlaps.T[0], max_overlaps.T[0]*100)):   
            print xy                                             # <--
            ax.annotate('%d' % max_overlaps.T[1][i], xy=xy)#, textcoords='offset points')
        ax.set_xticklabels(x_tick_labels)
        ax.set_xlabel("Interdomain distance ($\AA$)")
        ax.set_ylabel("Overlap %")
        plt.legend()
        plt.show()

    plot_fig(cum_overlaps_wr, sing_max_overlaps_wr, distances)
    plot_fig(cum_overlaps_wt, sing_max_overlaps_wt, distances)


def best_clustering_example_plots():
    dataset, _  = sklearn.datasets.make_circles(n_samples=1000, shuffle=False, noise= 0.05, factor=0.6)
    #colors = define_tableau20_cm()
    colors = sns.color_palette("hls")
    def plot_algorithm_results(alg_class, dataset, params):
        alg = alg_class(**params)
        alg.fit(dataset)
        if hasattr(alg, 'labels_'):
            y_pred = alg.labels_.astype(numpy.int)
        else:
            y_pred = alg.predict(dataset)
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.set_aspect(1)
        for i in range(params['n_clusters']):
            plt.scatter(dataset[y_pred == i, 0], dataset[y_pred == i, 1], 
                        color= colors[i], 
                        s=10)
        plt.xlim(-1.5, 1.5)
        plt.ylim(-1.5, 1.5)
        plt.show()
    plot_algorithm_results(sklearn.cluster.KMeans, dataset, {"n_clusters":2})
    plot_algorithm_results(sklearn.cluster.KMeans, dataset, {"n_clusters":3})
    plot_algorithm_results(sklearn.cluster.SpectralClustering, dataset, {"n_clusters":2, "affinity":"nearest_neighbors"})
    plot_algorithm_results(sklearn.cluster.SpectralClustering, dataset, {"n_clusters":3, "affinity":"nearest_neighbors"})

def initial_profile_plots():
    sns.set_style("whitegrid")
    #-------------------
    # Initial profiling
    #-------------------
    data = {
            "Size":[],
            "Solvent type":[], 
            "Function fam.":[],
            "Time (s)":[]
            }
    for size in _data.initial_profiling:
        for solv_type in _data.initial_profiling[size]:
            for ff in _data.initial_profiling[size][solv_type]:
                data["Size"].append(size)
                data["Solvent type"].append(solv_type)
                data["Function fam."].append(ff)
                data["Time (s)"].append(_data.initial_profiling[size][solv_type][ff])
    db = pd.DataFrame.from_dict(data)
    sns.barplot("Size", "Time (s)", hue = "Function fam.", data = db)
    
    plt.show()
    
def cuda_kernel_plots():
    sns.set_style("whitegrid")
    #-------------------
    # Initial profiling
    #-------------------
    data = {
            "Size":[],
            "P. Model":[], 
            "Function":[],
            "Speedup":[]
            }
    for size in _data.NB_kernel_speedup:
        for p_model in _data.NB_kernel_speedup[size]:
            for ff in _data.NB_kernel_speedup[size][p_model]:
                data["Size"].append(size)
                data["P. Model"].append(p_model)
                data["Function"].append(ff)
                data["Speedup"].append(_data.NB_kernel_speedup[size][p_model][ff])
    db = pd.DataFrame.from_dict(data)
    sns.barplot("Size", "Speedup", hue = "Function", data = db)
    plt.show()

if __name__ == '__main__':
    print data_path
#     plot_drug_cost_per_year()
#     plot_3adk_closing_cumulative_overlap()
#     best_clustering_example_plots()
    #initial_profile_plots()
    cuda_kernel_plots()
