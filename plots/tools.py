import csv
from _collections import defaultdict
import seaborn as sns

def define_tableau20_cm ():
    """
    http://www.randalolson.com/2014/06/28/how-to-make-beautiful-data-visualizations-in-python-with-matplotlib/
    """
    # These are the "Tableau 20" colors as RGB.  
    tableau20 = [(31, 119, 180), (174, 199, 232), (255, 127, 14), (255, 187, 120),  
                 (44, 160, 44), (152, 223, 138), (214, 39, 40), (255, 152, 150),  
                 (148, 103, 189), (197, 176, 213), (140, 86, 75), (196, 156, 148),  
                 (227, 119, 194), (247, 182, 210), (127, 127, 127), (199, 199, 199),  
                 (188, 189, 34), (219, 219, 141), (23, 190, 207), (158, 218, 229)]  
      
    # Scale the RGB values to the [0, 1] range, which is the format matplotlib accepts.  
    for i in range(len(tableau20)):  
        r, g, b = tableau20[i]  
        tableau20[i] = (r / 255., g / 255., b / 255.) 
    
    return tableau20


def to_number(s):
    try:
        return int(s)
    except:
        try:
            return float(s)
        except:
            return 0
        
def import_csv_data(csv_filename):
    with open(csv_filename) as csvfile:
        reader = csv.DictReader(csvfile)
        data = defaultdict(list)
        for row in reader:
            for key in row:
                data[key].append(to_number(row[key]))
    return  data

def sort_data_by_key(key, data, data_len):
    keys = sorted(list(set(data[key])))
    colors = sns.color_palette("hls", 8)
    color_by_key = dict([(t, colors[i]) for i,t in enumerate(keys)])
    data_by_key = defaultdict(lambda : defaultdict(list))
    for i in range(data_len):
        key_val = data[key][i]
        for another_key in data.keys():
            if another_key != key:
                data_by_key[key_val][another_key].append(data[another_key][i])
    return data_by_key, keys, color_by_key